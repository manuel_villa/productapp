﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ProductsApp.DataModel;

namespace ProductsApp.Controllers
{
    public class PersonaController : ApiController
    {
        private TestNetEntities db = new TestNetEntities();

        // GET api/Persona
        public IEnumerable<Persona> GetAllPersonas()
        {
            return db.Persona.AsEnumerable();
        }

        // GET api/Persona/5
        public IHttpActionResult GetPersona(int id)
        {
            Persona persona = db.Persona.Find(id);
            if (persona == null)
            {
                return NotFound();
            }

            return Ok(persona);
        }

        // PUT api/Persona/5
        public HttpResponseMessage PutPersona(int id, [FromBody] Persona persona)
        {
            if (ModelState.IsValid && id == persona.Id)
            {
                db.Entry(persona).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // POST api/Persona
        public HttpResponseMessage PostPersona(Persona persona)
        {
            if (ModelState.IsValid)
            {
                db.Persona.Add(persona);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, persona);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = persona.Id }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE api/Persona/5
        public HttpResponseMessage DeletePersona(int id)
        {
            Persona persona = db.Persona.Find(id);
            if (persona == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Persona.Remove(persona);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, persona);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}