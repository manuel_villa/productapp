﻿var url = "/api/persona/";

function getPersona()
{
    var idPersona = $("#IdPersonaBuscar").val();
    //var url = "/api/persona/"+idPersona;
    $.getJSON(url + idPersona)
     .complete(function (data) {
         var persona = JSON.parse(data.responseText);
         $("#Id").val(persona.Id);
         $("#Nombre").val(persona.Nombre);
         $("#Email").val(persona.Email);
         $("#Telefono").val(persona.Telefono);
         $("#Direccion").val(persona.Direccion);
         
      });
}

function getAllPersona() {
   // var url = "/api/persona/"
    $.getJSON(url)
     .complete(function (data) {
         var listPersonas = JSON.parse(data.responseText);
         $("#ListaPersonas").html("");
         $.each(listPersonas, function (index, item) {
             var pers = "<p> Id: " + item.Id + " Nombre: " + item.Nombre +"</p>";
             $("#ListaPersonas").append(pers);

         });
     });
}

function postPersona()
{
    //Creamos un objeto persona
    var id=$("#Id").val();
    var nombre=$("#Nombre").val();
    var email=  $("#Email").val();
    var telefono=$("#Telefono").val();
    var direccion= $("#Direccion").val();
    var persona = { Id: id, Nombre: nombre, Email: email, Telefono: telefono, Direccion: direccion };

    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(persona),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: true,
        success: function (data, status, jqXHR) {
            alert("Se ha guardado exitosamente");
            getAllPersona();
        },
        error: function (xhr) {
            alert(xhr.responseText);
        }
    });
}

function putPersona() {
    //Creamos un objeto persona
    var id = $("#Id").val();
    var nombre = $("#Nombre").val();
    var email = $("#Email").val();
    var telefono = $("#Telefono").val();
    var direccion = $("#Direccion").val();
    var persona = { Id: id, Nombre: nombre, Email: email, Telefono: telefono, Direccion: direccion };

    $.ajax({
        type: "POST",
        url: url,
        data: {"id":id,"persona": JSON.stringify(persona)},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: true,
        headers: {
            "Content-Type": "application/json",
            "X-HTTP-Method-Override": "PUT"
        },
        success: function (data, status, jqXHR) {
            alert("Se ha guardado exitosamente");
            getAllPersona();
        },
        error: function (xhr) {
            alert(xhr.responseText);
        }
    });
}